import json
import base64
import os
from .workspace_path import WorpsacePath


class Slot:
    def __init__(self, id: str, associated_plugin: str = ""):
        self.id = id
        self.associated_file_path = WorpsacePath.get_path() + id + ".slot"
        self.associated_plugin = associated_plugin
        self._create_if_empty()
        self.load_header()

    def get_id(self) -> str:
        return self.id

    def _create_if_empty(self):
        if not os.path.isfile(self.associated_file_path):
            with open(self.associated_file_path, "w") as F:
                json.dump({"associated_plugin": self.associated_plugin}, F)
                F.write("\n---data_section---\n")
                json.dump({"data": ""}, F)

    def delete_associated_file(self):
        if os.path.isfile(self.associated_file_path):
            os.remove(self.associated_file_path)

    def get_data(self) -> bytes:
        with open(self.associated_file_path, "r") as F:
            data = F.read().split("---data_section---")
            return base64.decodebytes(bytes(json.loads(data[1])["data"], encoding="UTF-8"))

    def write_data(self, in_data: bytes):
        with open(self.associated_file_path, "w") as F:
            F.write(json.dumps({"associated_plugin": self.associated_plugin}))
            F.write("\n---data_section---\n")
            data = base64.encodebytes(in_data).decode("UTF-8")
            F.write(json.dumps({"data": data}))

    def save_header(self):
        self._create_if_empty()

    def load_header(self):
        with open(self.associated_file_path, "r") as F:
            data = F.readline()
            self.associated_plugin = json.loads(data)["associated_plugin"]

    def __eq__(self, other):
        if other.__class__ is Slot:
            return self.id == other.get_id()
        elif other.__class__ is str:
            return self.id == other
        raise Exception(f"Cannot compare Slot to {type(other)}")

    def __hash__(self) -> int:
        return hash(self.id)

from .plugin import Plugin
from .plugin_spec import PluginSpecification
from .plugin_tab_factory import PluginTabFactory
from PyQt6.QtWidgets import QWidget
from workspace import WorkspaceManager


class PluginManager:
    plugins: dict[str, Plugin] = {}

    @staticmethod
    def get_plugin_by_id(id: str, search_only_from_active_plugins=True) -> Plugin or None:
        if id in PluginManager.plugins:
            plugin: Plugin = PluginManager.plugins[id]
            if plugin.is_active():
                return plugin
            elif not search_only_from_active_plugins:
                return plugin
            else:
                return None

    @staticmethod
    def load_from_module(module):
        for x in PluginManager.plugins.values():
            if x.is_active():
                x.deactivate()

        imported: list[Plugin] = [x() for x in getattr(module, "MRS_PLUGIN_CLASSES")]

        plugin_dict = {}
        for x in imported:
            plugin_dict[x.id] = x

        PluginManager.plugins = plugin_dict

        for plugin in PluginManager.plugins.values():
            spec = plugin.get_specification()
            for dep in spec.dependencies:
                if dep.id not in PluginManager.plugins:
                    raise Exception(f"Dependecy \"{dep.id}\" for \"{plugin.id}\" is missing")
                dep_plugin: Plugin = PluginManager.plugins[dep.id]
                dep_spec = dep_plugin.get_specification()
                if dep_spec.version != dep.version:
                    raise Exception(f"Dependecy \"{dep.id}\" for \"{plugin.id}\" is not the expected"
                                    f"version (got {dep_spec.version}, expected {dep.version})")

    @staticmethod
    def enable_plugin(id: str):
        if not PluginManager.plugins[id].is_active():
            PluginManager.plugins[id].activate()

    @staticmethod
    def disable_plugin(id: str):
        if PluginManager.plugins[id].is_active():
            PluginManager.plugins[id].deactivate()

    @staticmethod
    def is_plugin_active(id: str):
        return PluginManager.plugins[id].is_active()

    @staticmethod
    def invoke_plugin(id: str, method: str, *args, **kwargs):
        plugin= PluginManager.plugins[id]
        return plugin.invoke(method, *args, **kwargs)

    @staticmethod
    def disable_all_plugins():
        for plugin in PluginManager.plugins:
            PluginManager.disable_plugin(plugin)

    @staticmethod
    def get_plugin_for_file_type(extension: str):
        for plugin in PluginManager.plugins.values():
            spec = plugin.get_specification()
            if spec.supported_file_type == extension:
                return plugin

        return None

    @staticmethod
    def has_plugin(id: str):
        return id in PluginManager.plugins

    @staticmethod
    def get_specification_from_plugin(id: str) -> PluginSpecification:
        return PluginManager.plugins[id].get_specification()

    @staticmethod
    def create_tab(widget: QWidget, title: str = ""):
        PluginTabFactory.create_tab(widget, title)

    @staticmethod
    def get_tab_title(widget: QWidget) -> str:
        return PluginTabFactory.get_tab_title(widget)

    @staticmethod
    def set_tab_title(widget: QWidget, title: str):
        PluginTabFactory.set_tab_title(widget, title)

    @staticmethod
    def close_tab(widget: QWidget):
        PluginTabFactory.close_tab(widget)

    @staticmethod
    def save_to_slot(path: str, data: bytes):
        WorkspaceManager.traverse(path).write_data(data)
        WorkspaceManager.save_workspace()

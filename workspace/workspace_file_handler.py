import json
from .workspace import Workspace
from .document import Document
from .collection import Collection
from .page import Page
from .slot import Slot
from .workspace_path import WorpsacePath
import os
from pathlib import Path


class WorkpsaceFileHandler:

    @staticmethod
    def set_base_path(file_path: str):
        if "." in file_path:
            file_path = os.path.dirname(file_path)
        file_path = os.path.join(file_path, "")
        Path(file_path).mkdir(parents=True, exist_ok=True)
        WorpsacePath.set_path(file_path)

    @staticmethod
    def save_workspace(workspace: Workspace, filename: str):
        visited = {
            Document.__name__: set(),
            Collection.__name__: set(),
            Page.__name__: set(),
            Slot.__name__: set()
        }

        out_dict = {Slot.__name__: []}
        WorkpsaceFileHandler._save_workspace_helper(visited, workspace, out_dict)
        with open(WorpsacePath.get_path() + filename, "w") as F:
            json.dump(out_dict, F, indent=2)

    @staticmethod
    def _save_workspace_helper(visited: dict[str, set[str]], current_node, out_dict: dict):
        if current_node is None:
            return
        current_node_name = current_node.__class__.__name__
        if current_node.__class__ is Slot:
            if current_node.get_id() not in out_dict[Slot.__name__]:
                out_dict[Slot.__name__].append(current_node.get_id())
            return
        elif current_node.__class__ is not Workspace:
            if current_node.get_id() in visited[current_node_name]:
                return
            visited[current_node_name] = current_node.get_id()
            if current_node_name not in out_dict:
                out_dict[current_node_name] = {}
            out_dict[current_node_name][current_node.get_id()] = {"children": []}
            children: list[str] = out_dict[current_node_name][current_node.get_id()]["children"]
            if current_node.__class__ is Document:
                out_dict[current_node_name][current_node.get_id()]["owners"] = current_node.get_owners()
        else:
            out_dict[current_node_name] = {"children": []}
            children: list[str] = out_dict[current_node_name]["children"]

        children_obj = current_node.get_all_sub_elements()

        for child in children_obj:
            children.append(child.get_id())
            WorkpsaceFileHandler._save_workspace_helper(visited, child, out_dict)

    @staticmethod
    def load_workspace(filename: str) -> Workspace:
        nodes = {
            Document.__name__: {},
            Collection.__name__: {},
            Page.__name__: {},
            Slot.__name__: {}
        }

        with open(WorpsacePath.get_path() + filename, "r") as F:
            loaded = json.load(F)

        out = Workspace()

        for slot in loaded[Slot.__name__]:
            nodes[Slot.__name__][slot] = Slot(slot)

        for page in loaded[Page.__name__].items():
            slots = []
            for slot in page[1]["children"]:
                slots.append(nodes[Slot.__name__][slot])
            nodes[Page.__name__][page[0]] = Page(page[0], slots)

        for collection in loaded[Collection.__name__].items():
            pages = []
            for page in collection[1]["children"]:
                pages.append(nodes[Page.__name__][page])
            nodes[Collection.__name__][collection[0]] = Collection(collection[0], pages)

        for document in loaded[Document.__name__].items():
            collections = []
            for collection in document[1]["children"]:
                collections.append(nodes[Collection.__name__][collection])
            nodes[Document.__name__][document[0]] = Document(document[0], collections, document[1]["owners"])

        for document in loaded[Workspace.__name__]["children"]:
            out.add_document(nodes[Document.__name__][document])

        return out

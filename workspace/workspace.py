from .document import Document


class Workspace:
    def __init__(self, documents: list[Document] = None):
        self.documents: dict[str, Document] = {}
        if documents:
            for doc in documents:
                self.add_document(doc)

    def add_document(self, document: Document):
        if document.__class__ is not Document:
            raise ValueError(f"Cannot append {type(document)} to Workspace")
        self.documents[document.get_id()] = document

    def remove_document(self, id: str):
        self.documents.pop(id)

    def get_child(self, id: str) -> Document:
        return self.get_document(id)

    def get_document(self, id: str) -> Document:
        return self.documents[id]

    def get_ids_of_documents(self) -> list[str]:
        return [x for x in self.documents.keys()]

    def get_all_sub_elements(self) -> list[Document]:
        return [x for x in self.documents.values()]

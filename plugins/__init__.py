from plugin import Plugin
import importlib
import os
import inspect

SUPPORTED_FILE_TYPES = [".py"]
IGNORED_FOLDER_NAMES = ["__pycache__"]
IGNORED_FILE_NAMES = ["__init__.py"]


def get_includeables_from_module(module) -> list:
    includeables = []
    for key in dir(module):
        if not key.startswith("_"):
            includeables.append(key)
    return includeables


def get_all_plugin_derivatives(path: str, package: str) -> list:
    out = []
    module = importlib.import_module(path, package=package)
    for item in get_includeables_from_module(module):
        attr = getattr(module, item)
        if inspect.isclass(attr) and issubclass(attr, Plugin) and attr is not Plugin:
            out.append(attr)
    return out


def absolute_path_to_import_path(path: str, relative_to: str, package_to_exclude: str = "") -> str:
    directory = path.replace(relative_to, "")
    directory = directory[:directory.index(".")]
    directory = directory.replace("\\", "/").replace("/", ".")
    if package_to_exclude:
        directory = directory.replace("." + package_to_exclude, "")
    return directory


def get_list_of_supported_files(path: str) -> list:
    out = []
    for subdir, _, files in os.walk(path):
        if subdir not in IGNORED_FOLDER_NAMES:
            for file in files:
                if file not in IGNORED_FILE_NAMES:
                    abs_path = os.path.join(subdir, file)
                    extension = os.path.splitext(abs_path)[1]
                    if extension in SUPPORTED_FILE_TYPES:
                        out.append(abs_path)
    return out


def fetch_all_plugins(path: str, package: str) -> list:
    files = get_list_of_supported_files(path)
    out = []
    for file in files:
        try:
            module_path = absolute_path_to_import_path(file, path)
            plugins = get_all_plugin_derivatives(module_path, package)
            out.extend(plugins)
        except Exception as e:
            print(e)

    return out


MRS_PLUGIN_CLASSES = fetch_all_plugins(__path__[0], __package__)

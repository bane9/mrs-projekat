from PyQt6.QtWidgets import QMainWindow
from PyQt6 import uic
from PyQt6.QtGui import QShortcut, QKeySequence
from PyQt6.QtCore import pyqtSignal
import os
from plugin import PluginManager
from PyQt6.QtWidgets import QFontDialog


class TXTView(QMainWindow):
    on_close_signal = pyqtSignal(str, name="on_close_signal")
    on_save_signal = pyqtSignal(str, name="on_save_signal")

    def __init__(self, text: str = ""):
        super(TXTView, self).__init__(None)
        uic.loadUi(os.path.join(os.path.dirname(__file__),  'txt_ui.ui'), self)
        self.main_text_edit.insertPlainText(text)
        self.action_save.triggered.connect(self.save)
        self.action_set_font.triggered.connect(self.select_font)
        self.main_text_edit.textChanged.connect(self.add_asterisk)
        self.ctrl_s_handler = QShortcut(QKeySequence("Ctrl+S"), self.main_text_edit)
        self.ctrl_s_handler.activated.connect(self.save)
        PluginManager.create_tab(self, "Text document")

    def add_asterisk(self):
        current_title = PluginManager.get_tab_title(self)
        if not current_title.endswith(" (*)"):
            current_title += " (*)"
        PluginManager.set_tab_title(self, current_title)

    def remove_asterisk(self):
        current_title = PluginManager.get_tab_title(self)
        current_title = current_title.rstrip(" (*)")
        PluginManager.set_tab_title(self, current_title)

    def save(self):
        self.on_save_signal.emit(self.property("slot_path"))

    def select_font(self):
        font_select = QFontDialog()
        font, ok = font_select.getFont()

        if ok:
            self.main_text_edit.setFont(font)

    def close(self):
        PluginManager.close_tab(self)
        self.on_close_signal.emit(self.property("slot_path"))
        super(TXTView, self).close()

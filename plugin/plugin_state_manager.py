from .plugin_manager import PluginManager
from .plugin_spec import PluginSpecification
import json
import os
import sys
from accounts import AccountManager

sys.path.append(os.path.join(__file__, "././"))
import plugins


class PluginStateManager:
    state_file_name = "plugin_state.json"
    state: dict[str, bool] = None

    @staticmethod
    def init():
        PluginManager.load_from_module(plugins)
        PluginStateManager._load()
        remove_later: list[str] = []
        for x in PluginStateManager.state.keys():
            if not PluginManager.has_plugin(x):
                remove_later.append(x)
        for x in remove_later:
            PluginStateManager.state.pop(x)
        PluginStateManager._save()

    @staticmethod
    def _raise_if_not_admin():
        if not AccountManager.is_current_account_admin():
            raise Exception("Insufficient privileges")

    @staticmethod
    def enable_plugin(id: str):
        PluginStateManager._raise_if_not_admin()
        PluginManager.enable_plugin(id)
        PluginStateManager.state[id] = True
        PluginStateManager._save()

    @staticmethod
    def disable_plugin(id: str):
        PluginStateManager._raise_if_not_admin()
        PluginManager.disable_plugin(id)
        PluginStateManager.state[id] = False
        PluginStateManager._save()

    @staticmethod
    def is_plugin_enabled(id: str) -> bool:
        return PluginStateManager.state[id]

    @staticmethod
    def invoke_plugin(id: str, method: str, *args, **kwargs):
        return PluginManager.invoke_plugin(id, method, *args, **kwargs)

    @staticmethod
    def get_ids_of_all_plugins() -> list[str]:
        return [x for x in PluginStateManager.state.keys()]

    @staticmethod
    def get_ids_of_active_plugins() -> list[str]:
        return [x[0] for x in PluginStateManager.state.items() if x[1]]

    @staticmethod
    def get_ids_of_inactive_plugins() -> list[str]:
        return [x[0] for x in PluginStateManager.state.items() if not x[1]]

    @staticmethod
    def get_specification_from_plugin(id: str) -> PluginSpecification:
        return PluginManager.get_specification_from_plugin(id)

    @staticmethod
    def _load():
        if not os.path.exists(PluginStateManager.state_file_name):
            state = {}
            for x in PluginManager.plugins.keys():
                state[x] = False
            PluginStateManager.state = state
            PluginStateManager._save()
            return
        with open(PluginStateManager.state_file_name, "r") as F:
            state_json: dict = json.load(F)

        pop_later = []

        for k, v in state_json.items():
            if not PluginManager.get_plugin_by_id(k, False):
                pop_later.append(k)
            elif v:
                PluginManager.enable_plugin(k)

        for x in pop_later:
            state_json.pop(k)

        for x in PluginManager.plugins.keys():
            if x not in state_json:
                state_json[x] = False

        PluginStateManager.state = state_json

    @staticmethod
    def _save():
        with open(PluginStateManager.state_file_name, "w") as F:
            json.dump(PluginStateManager.state, F)

from PyQt6.QtCore import QObject, pyqtSignal
from .view import TXTView


class TXTController(QObject):
    tab_save_signal = pyqtSignal(str, bytes, name="tab_save_signal")

    def __init__(self, parent: QObject):
        super(TXTController, self).__init__(parent)
        self.views: dict[str, TXTView] = {}
    
    def new_view(self, slot_path: str, slot_data: bytes):
        view = TXTView(slot_data.decode("utf-8"))
        view.setProperty("slot_path", slot_path)
        view.on_close_signal.connect(self.on_tab_close)
        view.on_save_signal.connect(self.save_view_contents)
        self.views[slot_path] = view

    def save_view_contents(self, slot_path: str):
        text: str = self.views[slot_path].main_text_edit.toPlainText()
        self.tab_save_signal.emit(slot_path, text.encode("utf-8"))
        self.views[slot_path].remove_asterisk()

    def on_tab_close(self, slot_path: str):
        self.save_view_contents(slot_path)
        self.views.pop(slot_path)

    def close_view(self, slot_path: str):
        self.views[slot_path].close()

    def close_all_views(self):
        for view in [x for x in self.views.values()]:
            view.close()

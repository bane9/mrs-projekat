from plugin import Plugin, PluginManager
from plugin import PluginSpecification, Dependancy


class JPGPlugin(Plugin):
    def get_specification(self) -> PluginSpecification:
        spec = PluginSpecification()
        spec.version = "1.0.0"
        spec.supported_file_type = "jpg"

        return spec

from .workspace_file_handler import WorkpsaceFileHandler
from .workspace import Workspace
import os


class WorkspaceManager:
    workspace: Workspace = None
    workspace_filename = ""

    @classmethod
    def set_workspace_filepath(cls, path: str):
        WorkpsaceFileHandler.set_base_path(path)
        cls.workspace_filename = os.path.basename(path)

    @classmethod
    def set_workspace_object(cls, workspace: Workspace, path: str):
        cls.set_workspace_filepath(path)
        cls.workspace = workspace

    @classmethod
    def load_workspace(cls):
        WorkspaceManager.workspace = WorkpsaceFileHandler.load_workspace(cls.workspace_filename)

    @classmethod
    def get_workspace(cls) -> Workspace:
        return cls.workspace

    @classmethod
    def save_workspace(cls):
        WorkpsaceFileHandler.save_workspace(cls.workspace, cls.workspace_filename)

    @classmethod
    def traverse(cls, path: str):
        current = cls.workspace
        for x in path.split("."):
            current = current.get_child(x)
        return current

from PyQt6.QtWidgets import QMainWindow, QTabWidget
from PyQt6 import uic
import os
from plugin import PluginTabFactory


class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__(None)
        uic.loadUi(os.path.join(os.path.dirname(__file__),  'main_window.ui'), self)
        tab_widget: QTabWidget = self.main_tab_widget
        tab_widget.clear()
        PluginTabFactory.set_tab_widget(tab_widget)

from .plugin_spec import PluginSpecification
from PyQt6.QtCore import QObject


class Plugin(QObject):
    def __init__(self):
        super(Plugin, self).__init__(None)
        self.active = False

    def activate(self):
        self.active = True

    def deactivate(self):
        self.active = False

    def invoke(self, method: str, *args, **kwargs):
        if not self.is_active():
            raise Exception(f"Called inactive plugin {self.id}")
        return getattr(self, method)(*args, **kwargs)

    def is_active(self) -> bool:
        return self.active

    @classmethod
    @property
    def id(cls) -> str:
        return cls.__name__

    def __eq__(self, other):
        if other.__class__ is Plugin or issubclass(other.__class__, Plugin):
            return self.get_id() == other.get_id()
        elif other.__class__ is str:
            return self.get_id() == other
        raise Exception(f"Cannot compare Plugin to {type(other)}")

    def __hash__(self) -> int:
        return hash(self.get_id())

    def get_specification(self) -> PluginSpecification:
        return PluginSpecification()

from .page import Page


class Collection:
    def __init__(self, id: str, pages: list[Page] = None):
        self.id = id
        self.pages: dict[str, Page] = {}
        if pages:
            for page in pages:
                self.add_page(page)

    def get_id(self) -> str:
        return self.id

    def add_page(self, page: Page):
        if page.__class__ is not Page:
            raise ValueError(f"Cannot append {type(page)} to Collection")
        self.pages[page.get_id()] = page

    def get_child(self, id: str) -> Page:
        return self.get_page(id)

    def remove_page(self, id: str):
        self.pages.pop(id)

    def get_page(self, id: str) -> Page:
        return self.pages[id]

    def get_ids_of_pages(self) -> list[str]:
        return [x for x in self.pages.keys()]

    def get_all_sub_elements(self) -> list[Page]:
        return [x for x in self.pages.values()]

    def __contains__(self, element) -> bool:
        if element.__class__ is Page:
            return element.get_id() in self.pages
        elif element.__class__ is str:
            return element in self.pages
        raise Exception(f"Cannot cannot check if {type(element)} is in Collection")

    def __eq__(self, other):
        if other.__class__ is Collection:
            return self.id == other.get_id()
        elif other.__class__ is str:
            return self.id == other
        raise Exception(f"Cannot compare Collection to {type(other)}")

    def __hash__(self) -> int:
        return hash(self.id)

from .slot import Slot


class Page:
    def __init__(self, id: str, slots: list[Slot] = None):
        self.id = id
        self.slots: dict[str, Slot] = {}
        if slots:
            for slot in slots:
                self.add_slot(slot)

    def get_id(self) -> str:
        return self.id

    def add_slot(self, slot: Slot):
        if slot.__class__ is not Slot:
            raise ValueError(f"Cannot append {type(slot)} to Page")
        self.slots[slot.get_id()] = slot

    def remove_slot(self, id: str):
        self.slots.pop(id)

    def get_child(self, id: str) -> Slot:
        return self.get_slot(id)

    def get_slot(self, id: str) -> Slot:
        return self.slots[id]

    def get_ids_of_slots(self) -> list[str]:
        return [x for x in self.slots.keys()]

    def get_all_sub_elements(self) -> list[Slot]:
        return [x for x in self.slots.values()]

    def __contains__(self, element) -> bool:
        if element.__class__ is Slot:
            return element.get_id() in self.slots
        elif element.__class__ is str:
            return element in self.slots
        raise Exception(f"Cannot cannot check if {type(element)} is in Page")

    def __eq__(self, other):
        if other.__class__ is Page:
            return self.id == other.get_id()
        elif other.__class__ is str:
            return self.id == other
        raise Exception(f"Cannot compare Page to {type(other)}")

    def __hash__(self) -> int:
        return hash(self.id)

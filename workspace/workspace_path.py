class WorpsacePath:
    CURRENT_PATH: str = ""

    @staticmethod
    def get_path() -> str:
        return WorpsacePath.CURRENT_PATH

    @staticmethod
    def set_path(path: str) -> str:
        WorpsacePath.CURRENT_PATH = path

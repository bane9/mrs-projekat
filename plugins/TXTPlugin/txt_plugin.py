from plugin import Plugin
from plugin import PluginManager
from plugin import PluginSpecification, Author
from .controller import TXTController


class TXTPlugin(Plugin):
    def __init__(self):
        super(TXTPlugin, self).__init__()
        self.controller = TXTController(self)
        self.controller.tab_save_signal.connect(self.save)

    def open(self, slot_path: str, slot_data: bytes):
        self.controller.new_view(slot_path, slot_data)

    def save(self, slot_path: str, slot_data: bytes):
        PluginManager.save_to_slot(slot_path, slot_data)

    def close(self, slot_path: str):
        self.controller.close_view(slot_path)

    def close_all(self):
        self.controller.close_all_views()

    def deactivate(self):
        self.close_all()
        super(TXTPlugin, self).deactivate()

    def get_specification(self) -> PluginSpecification:
        spec = PluginSpecification()
        spec.name = "Text document handler"
        spec.version = "1.0.0"
        spec.core_version = "1.0.0"
        spec.category = "Text Utility"
        spec.license = "MIT"
        spec.description = "Text document handler"
        spec.supported_file_type = "txt"
        spec.can_create_new = True
        spec.authors.append(Author("Branislav", "Brzak", "branislav.brzak.18@singimail.rs"))
        
        return spec

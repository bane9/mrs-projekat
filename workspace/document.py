from .collection import Collection


class Document:
    def __init__(self, id: str, collections: list[Collection] = None, owners: list[int] = None):
        self.id = id
        self.collections: dict[str, Collection] = {}
        self.owners: list[int] = []
        if collections:
            for col in collections:
                self.add_collection(col)
        if owners:
            for owner in owners:
                self.add_owner(owner)

    def add_owner(self, owner_id: int):
        if owner_id not in self.owners:
            self.owners.append(owner_id)

    def remove_owner(self, owner_id: int):
        if owner_id in self.owners:
            self.owners.pop(self.owners.index(owner_id))
    
    def has_owner(self, owner_id: int) -> bool:
        return owner_id in self.owners

    def get_child(self, id: str) -> Collection:
        return self.get_collection(id)
    
    def get_owners(self) -> list[int]:
        return self.owners.copy()

    def get_id(self) -> str:
        return self.id

    def add_collection(self, collection: Collection):
        if collection.__class__ is not Collection:
            raise ValueError(f"Cannot append {type(collection)} to Documents")
        self.collections[collection.get_id()] = collection

    def remove_collection(self, id: str):
        self.collections.pop(id)

    def get_collection(self, id: str) -> Collection:
        return self.collections[id]

    def get_ids_of_collections(self) -> list[str]:
        return [x for x in self.collections.keys()]

    def get_all_sub_elements(self) -> list[Collection]:
        return [x for x in self.collections.values()]

    def __eq__(self, other):
        if other.__class__ is Document:
            return self.id == other.get_id()
        elif other.__class__ is str:
            return self.id == other
        raise Exception(f"Cannot compare Document to {type(other)}")

    def __contains__(self, element) -> bool:
        if element.__class__ is Collection:
            return element.get_id() in self.collections
        elif element.__class__ is str:
            return element in self.collections
        raise Exception(f"Cannot cannot check if {type(element)} is in Document")

    def __hash__(self) -> int:
        return hash(self.id)

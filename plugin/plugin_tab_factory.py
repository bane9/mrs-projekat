from PyQt6.QtWidgets import QTabWidget, QWidget

class PluginTabFactory:
    tab_widget: QTabWidget = None

    @classmethod
    def set_tab_widget(cls, tab_widget: QTabWidget):
        cls.tab_widget = tab_widget

    @classmethod
    def create_tab(cls, widget: QWidget, title: str = ""):
        cls.tab_widget.addTab(widget, title)

    @classmethod
    def get_tab_title(cls, widget: QWidget) -> str:
        index = cls.tab_widget.indexOf(widget)
        return cls.tab_widget.tabText(index)

    @classmethod
    def set_tab_title(cls, widget: QWidget, title: str):
        index = cls.tab_widget.indexOf(widget)
        cls.tab_widget.setTabText(index, title)

    @classmethod
    def close_tab(cls, widget: QWidget):
        cls.tab_widget.removeTab(cls.tab_widget.indexOf(widget))

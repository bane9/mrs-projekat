class Author:
    def __init__(self, first_name: str, last_name: str, email: str = "", web_page: str = ""):
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.web_page = web_page


class Dependancy:
    def __init__(self, id: str, version: str):
        self.id = id
        self.version = version


class PluginSpecification:
    def __init__(self):
        self.name = ""
        self.version = ""
        self.core_version = ""
        self.category = ""
        self.license = ""
        self.description = ""
        self.web_page = ""
        self.supported_file_type = ""
        self.can_create_new = False
        self.authors: list[Author] = []
        self.dependencies: list[Dependancy] = []

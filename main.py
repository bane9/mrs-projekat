from gui import MainWindow
from PyQt6.QtWidgets import QApplication
import sys
from plugin import PluginStateManager
from accounts import AccountManager
from workspace import Slot, Page, Collection, Workspace, Document, WorkspaceManager


def main():
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()

    AccountManager.login("admin", "admin")

    workspace = Workspace()

    WorkspaceManager.set_workspace_object(workspace, "test_wsp/idk.workspace")

    slot1 = Slot("slot1", "plugin1")
    slot2 = Slot("slot2", "plugin2")
    page1 = Page("page1", [slot1])
    page2 = Page("page2", [slot1, slot2])
    col1 = Collection("col1", [page1])
    col2 = Collection("col2", [page1, page2])
    doc1 = Document("doc1", [col1])
    doc1.add_owner(1)
    doc2 = Document("doc2", [col1, col2])
    doc2.add_owner(1)
    doc2.add_owner(2)
    workspace.add_document(doc1)
    workspace.add_document(doc2)

    PluginStateManager.enable_plugin("TXTPlugin")
    PluginStateManager.invoke_plugin("TXTPlugin", "open", "doc1.col1.page1.slot1", slot1.get_data())
    PluginStateManager.invoke_plugin("TXTPlugin", "open", "doc2.col2.page2.slot2", slot2.get_data())

    sys.exit(app.exec())
    
if __name__ == "__main__":
    main()

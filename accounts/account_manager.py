from .account import Account
import json
import os
import random


class AccountManager:
    account_file_name = "accounts.json"
    accounts: dict[int, Account] = {}
    current_account: Account = None

    @staticmethod
    def init():
        if os.path.isfile(AccountManager.account_file_name):
            with open(AccountManager.account_file_name, "r") as F:
                accs_json: dict[int, dict] = json.load(F)
                for id, account in accs_json.items():
                    id = int(id)
                    AccountManager.accounts[id] = Account(id, account["username"],
                                                          account["password"], account["is_admin"])
        if not AccountManager.contains_username("admin"):
            AccountManager.create_account("admin", "admin", True)
            AccountManager.save()

    @staticmethod
    def save():
        acc_json = {}
        for id, account in AccountManager.accounts.items():
            acc_json[id] = {"username": account.username, "password": account.password, "is_admin": account.is_admin}
        with open(AccountManager.account_file_name, "w") as F:
            json.dump(acc_json, F)

    @staticmethod
    def get_id_from_username(username: str) -> int or None:
        for id, account in AccountManager.accounts.items():
            if account.username == username:
                return id
        return None

    @staticmethod
    def contains_username(username: str) -> bool:
        return AccountManager.get_id_from_username(username) is not None

    @staticmethod
    def contains_id(id: str) -> bool:
        return id in AccountManager.accounts

    @staticmethod
    def login(username: str, password: str):
        id = AccountManager.get_id_from_username(username)
        if id is None:
            raise Exception("Wrong login info")
        elif AccountManager.accounts[id].password != password:
            raise Exception("Wrong login info")
        AccountManager.current_account = AccountManager.accounts[id]

    @staticmethod
    def get_account_from_id(id: int) -> Account:
        return AccountManager.accounts[id]

    @staticmethod
    def get_account_from_username(username: str) -> Account:
        return AccountManager.accounts[AccountManager.get_id_from_username(username)]

    @staticmethod
    def modify_account(id: int, usenrame: str, password: str, is_admin: bool):
        if AccountManager.get_logged_in_account() is not None and not AccountManager.is_current_account_admin():
            raise Exception("Insufficient permissions")
        check_id = AccountManager.get_id_from_username(usenrame)
        if check_id is not None and check_id != id:
            raise Exception("Username already in use by a different account")
        if not usenrame:
            raise Exception("Username cannot be emtpy")
        if not password:
            raise Exception("Password cannot be empty")
        acc = Account(id, usenrame, password, is_admin)
        AccountManager.accounts[id] = acc
        AccountManager.save()
        if AccountManager.current_account is not None and AccountManager.current_account.id == id:
            AccountManager.current_account = acc

    @staticmethod
    def create_account(usenrame: str, password: str, is_admin: bool):
        id = random.randint(0, 10000)
        while id in AccountManager.accounts:
            id = random.randint(0, 10000)

        AccountManager.modify_account(id, usenrame, password, is_admin)

    @staticmethod
    def remove_account(id: str):
        AccountManager.accounts.pop(id)
        AccountManager.save()

    @staticmethod
    def get_logged_in_account() -> Account:
        return AccountManager.current_account

    @staticmethod
    def is_current_account_admin() -> bool:
        return False if not AccountManager.current_account else AccountManager.current_account.is_admin
